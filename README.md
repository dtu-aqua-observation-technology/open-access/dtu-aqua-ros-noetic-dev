# Pre-installation checks

1. Make sure you have installed [docker desktop](https://docs.docker.com/desktop/install/linux-install/) NB: Ubuntu 24.04 has trouble with docker desktop for the moment. You can still use docker CLI to do everything you need.
2. This README is for linux users only at the moment. Windows & Mac sections will be added as they arise.

# Using the pre-made images

With docker CLI, run: `docker pull ffthompson/ros:latest-aqua-noetic-pydev`

# Installation

From this directory, run: `bash install_script.sh`

# Using a container

## CLI

From terminal run the following: `docker run -it ffthompson/ros:latest-aqua-noetic-pydev`. You will be output to a `/bin/bash` terminal with the ROS noetic environment ready to go.

Run a new container as follows: `docker run -it --name noetic-pydev ffthompson/ros:latest-aqua-noetic-pydev`.

Start or stop the container with `docker start noetic-pydev` or `docker stop noetic-pydev` respectively.

With the docker container running, run the following in a new terminal: `docker exec -it noetic-pydev /bin/bash`

This will bring up the container's environment in /home/ubuntu as the ubuntu user.

Alternatively, you may wish to set up the image with an IDE like PyCharm or VSCode for specific development projects. The image has been tested specifically with PyCharm and works fine, but may need further environment setup if you intend on developing catkin workspace packages.

## Docker Desktop

Locate the `ffthompson/ros:latest-aqua-noetic-pydev` and click the play button as shown.

![Run container](Assets/image.png)

# Where to now?

The perception configuration build of ROS noetic is setup in /opt/ros/noetic. Additional packages from [blue3_support_pkgs](https://gitlab.gbar.dtu.dk/dtu-aqua-observation-technology/open-access/blue3_support_pkgs) and [kalibr](https://github.com/ethz-asl/kalibr/wiki) are also installed for convenience.

You can add a shared location to between your host machine and the docker container by adding `-v location/to/share/host:location/to/share/docker`. This will bind mount a volume to the specified folder on your host machine with the host. This allows fast copy and paste of data to / from the docker workspace. **Note**: the pre-built image assumes your host user has a uid and gid of 1000. If not, then build the image from this repository using ./install_script.sh which will set your built image with your host user's uid and guid. Be aware that everything in your shared folder is mutable from the host and the docker. If you want to hide things from your host machine, work in a different folder within your docker instance.

Otherwise, create a new bash terminal as per the previous step for each process you want to run. Make sure to stop the container when you're finished working.
